// TODO: 
// - fonctions pour factoriser la sélection d'un élément dans le DOM pour l.12 21 etc
// - utiliser className au lieu de classList

function html(cssSelector){
    return document.getElementById(cssSelector);
}

document.addEventListener( 'DOMContentLoaded', function() { 
    let key1 = html('piano-key-1');

    let content1 = html('content-mon-projet');
    let content2 = html('content-technologies');
    let content3 = html('content-formation');
    let content4 = html('content-experience');
    let content5 = html('content-realisations');
    let content6 = html('content-disponibilites');
    let content7 = html('content-coordonnees');

    key1.addEventListener( 'click', function() {
       let isContent1Hidden = content1.classList.contains( 'hidden' );
       let isContent2Hidden = content2.classList.contains( 'hidden' );
       let isContent3Hidden = content3.classList.contains( 'hidden' );
       let isContent4Hidden = content4.classList.contains( 'hidden' );
       let isContent5Hidden = content5.classList.contains( 'hidden' );
       let isContent6Hidden = content6.classList.contains( 'hidden' );
       let isContent7Hidden = content7.classList.contains( 'hidden' );

        if( isContent1Hidden ) {
            content1.classList.remove( 'hidden' );
        }
        else {
            content1.classList.add( 'hidden' );
        }

        if( !isContent2Hidden ) {
            content2.classList.add( 'hidden' );
        }

        if( !isContent3Hidden ) {
            content3.classList.add( 'hidden' );
        }

        if( !isContent4Hidden ) {
            content4.classList.add( 'hidden' );
        }

        if( !isContent5Hidden ) {
            content5.classList.add( 'hidden' );
        }

        if( !isContent6Hidden ) {
            content6.classList.add( 'hidden' );
        }

        if( !isContent7Hidden ) {
            content7.classList.add( 'hidden' );
        }
    });
});

document.addEventListener( 'DOMContentLoaded', function() { 
    let key2 = html('piano-key-2');

    let content1 = html('content-mon-projet');
    let content2 = html('content-technologies');
    let content3 = html('content-formation');
    let content4 = html('content-experience');
    let content5 = html('content-realisations');
    let content6 = html('content-disponibilites');
    let content7 = html('content-coordonnees');

    key2.addEventListener( 'click', function() {
       let isContent1Hidden = content1.classList.contains( 'hidden' );
       let isContent2Hidden = content2.classList.contains( 'hidden' );
       let isContent3Hidden = content3.classList.contains( 'hidden' );
       let isContent4Hidden = content4.classList.contains( 'hidden' );
       let isContent5Hidden = content5.classList.contains( 'hidden' );
       let isContent6Hidden = content6.classList.contains( 'hidden' );
       let isContent7Hidden = content7.classList.contains( 'hidden' );
       
        if( isContent2Hidden ) {
            content2.classList.remove( 'hidden' );
        }
        else {
            content2.classList.add( 'hidden' );
        }

        if( !isContent1Hidden ) {
            content1.classList.add( 'hidden' );
        }

        if( !isContent3Hidden ) {
            content3.classList.add( 'hidden' );
        }

        if( !isContent4Hidden ) {
            content4.classList.add( 'hidden' );
        }

        if( !isContent5Hidden ) {
            content5.classList.add( 'hidden' );
        }

        if( !isContent6Hidden ) {
            content6.classList.add( 'hidden' );
        }

        if( !isContent7Hidden ) {
            content7.classList.add( 'hidden' );
        }
    });
});

document.addEventListener( 'DOMContentLoaded', function() { 
    let key3 = html('piano-key-3');

    let content1 = html('content-mon-projet');
    let content2 = html('content-technologies');
    let content3 = html('content-formation');
    let content4 = html('content-experience');
    let content5 = html('content-realisations');
    let content6 = html('content-disponibilites');
    let content7 = html('content-coordonnees');

    key3.addEventListener( 'click', function() {
        let isContent1Hidden = content1.classList.contains( 'hidden' );
        let isContent2Hidden = content2.classList.contains( 'hidden' );
        let isContent3Hidden = content3.classList.contains( 'hidden' );
        let isContent4Hidden = content4.classList.contains( 'hidden' );
        let isContent5Hidden = content5.classList.contains( 'hidden' );
        let isContent6Hidden = content6.classList.contains( 'hidden' );
        let isContent7Hidden = content7.classList.contains( 'hidden' );

        if( isContent3Hidden ) {
            content3.classList.remove( 'hidden' );
        }
        else {
            content3.classList.add( 'hidden' );
        }
     
        if( !isContent1Hidden ) {
            content1.classList.add( 'hidden' );
        }

        if( !isContent2Hidden ) {
            content2.classList.add( 'hidden' );
        }

        if( !isContent4Hidden ) {
            content4.classList.add( 'hidden' );
        }

        if( !isContent5Hidden ) {
            content5.classList.add( 'hidden' );
        }

        if( !isContent6Hidden ) {
            content6.classList.add( 'hidden' );
        }

        if( !isContent7Hidden ) {
            content7.classList.add( 'hidden' );
        }
    });
});

document.addEventListener( 'DOMContentLoaded', function() { 
    let key4 = html('piano-key-4');

    let content1 = html('content-mon-projet');
    let content2 = html('content-technologies');
    let content3 = html('content-formation');
    let content4 = html('content-experience');
    let content5 = html('content-realisations');
    let content6 = html('content-disponibilites');
    let content7 = html('content-coordonnees');

    key4.addEventListener( 'click', function() {
        let isContent1Hidden = content1.classList.contains( 'hidden' );
        let isContent2Hidden = content2.classList.contains( 'hidden' );
        let isContent3Hidden = content3.classList.contains( 'hidden' );
        let isContent4Hidden = content4.classList.contains( 'hidden' );
        let isContent5Hidden = content5.classList.contains( 'hidden' );
        let isContent6Hidden = content6.classList.contains( 'hidden' );
        let isContent7Hidden = content7.classList.contains( 'hidden' );     

        if( isContent4Hidden ) {
            content4.classList.remove( 'hidden' );
        }
        else {
            content4.classList.add( 'hidden' );
        }
        
        if( !isContent1Hidden ) {
            content1.classList.add( 'hidden' );
        }
        
        if( !isContent2Hidden ) {
            content2.classList.add( 'hidden' );
        }

        if( !isContent3Hidden ) {
            content3.classList.add( 'hidden' );
        }

        if( !isContent5Hidden ) {
            content5.classList.add( 'hidden' );
        }

        if( !isContent6Hidden ) {
            content6.classList.add( 'hidden' );
        }

        if( !isContent7Hidden ) {
            content7.classList.add( 'hidden' );
        }
    });
});

document.addEventListener( 'DOMContentLoaded', function() { 
    let key5 = html('piano-key-5');

    let content1 = html('content-mon-projet');
    let content2 = html('content-technologies');
    let content3 = html('content-formation');
    let content4 = html('content-experience');
    let content5 = html('content-realisations');
    let content6 = html('content-disponibilites');
    let content7 = html('content-coordonnees');

    key5.addEventListener( 'click', function() {
       let isContent1Hidden = content1.classList.contains( 'hidden' );
       let isContent2Hidden = content2.classList.contains( 'hidden' );
       let isContent3Hidden = content3.classList.contains( 'hidden' );
       let isContent4Hidden = content4.classList.contains( 'hidden' );
       let isContent5Hidden = content5.classList.contains( 'hidden' );
       let isContent6Hidden = content6.classList.contains( 'hidden' );
       let isContent7Hidden = content7.classList.contains( 'hidden' );

        if( isContent5Hidden ) {
            content5.classList.remove( 'hidden' );
        }
        else {
            content5.classList.add( 'hidden' );
        }

        if( !isContent1Hidden ) {
            content1.classList.add( 'hidden' );
        }
        
        if( !isContent2Hidden ) {
            content2.classList.add( 'hidden' );
        }

        if( !isContent3Hidden ) {
            content3.classList.add( 'hidden' );
        }

        if( !isContent4Hidden ) {
            content4.classList.add( 'hidden' );
        }

        if( !isContent6Hidden ) {
            content6.classList.add( 'hidden' );
        }

        if( !isContent7Hidden ) {
            content7.classList.add( 'hidden' );
        }
    });
});

document.addEventListener( 'DOMContentLoaded', function() { 
    let key6 = html('piano-key-6');

    let content1 = html('content-mon-projet');
    let content2 = html('content-technologies');
    let content3 = html('content-formation');
    let content4 = html('content-experience');
    let content5 = html('content-realisations');
    let content6 = html('content-disponibilites');
    let content7 = html('content-coordonnees');

    key6.addEventListener( 'click', function() {
        let isContent1Hidden = content1.classList.contains( 'hidden' );
        let isContent2Hidden = content2.classList.contains( 'hidden' );
        let isContent3Hidden = content3.classList.contains( 'hidden' );
        let isContent4Hidden = content4.classList.contains( 'hidden' );
        let isContent5Hidden = content5.classList.contains( 'hidden' );
        let isContent6Hidden = content6.classList.contains( 'hidden' );
        let isContent7Hidden = content7.classList.contains( 'hidden' );

        if( isContent6Hidden ) {
            content6.classList.remove( 'hidden' );
        }
        else {
            content6.classList.add( 'hidden' );
        }
        
        if( !isContent1Hidden ) {
            content1.classList.add( 'hidden' );
        }
        
        if( !isContent2Hidden ) {
            content2.classList.add( 'hidden' );
        }

        if( !isContent3Hidden ) {
            content3.classList.add( 'hidden' );
        }

        if( !isContent4Hidden ) {
            content4.classList.add( 'hidden' );
        }

        if( !isContent5Hidden ) {
            content5.classList.add( 'hidden' );
        }

        if( !isContent7Hidden ) {
            content7.classList.add( 'hidden' );
        }
    });
});

document.addEventListener( 'DOMContentLoaded', function() { 
    let key7 = html('piano-key-7');

    let content1 = html('content-mon-projet');
    let content2 = html('content-technologies');
    let content3 = html('content-formation');
    let content4 = html('content-experience');
    let content5 = html('content-realisations');
    let content6 = html('content-disponibilites');
    let content7 = html('content-coordonnees');

    key7.addEventListener( 'click', function() {
       let isContent1Hidden = content1.classList.contains( 'hidden' );
       let isContent2Hidden = content2.classList.contains( 'hidden' );
       let isContent3Hidden = content3.classList.contains( 'hidden' );
       let isContent4Hidden = content4.classList.contains( 'hidden' );
       let isContent5Hidden = content5.classList.contains( 'hidden' );
       let isContent6Hidden = content6.classList.contains( 'hidden' );
       let isContent7Hidden = content7.classList.contains( 'hidden' );
       
        if( isContent7Hidden ) {
            content7.classList.remove( 'hidden' );
        }
        else {
            content7.classList.add( 'hidden' );
        }
        
        if( !isContent1Hidden ) {
            content1.classList.add( 'hidden' );
        }
        
        if( !isContent2Hidden ) {
            content2.classList.add( 'hidden' );
        }

        if( !isContent3Hidden ) {
            content3.classList.add( 'hidden' );
        }

        if( !isContent4Hidden ) {
            content4.classList.add( 'hidden' );
        }

        if( !isContent5Hidden ) {
            content5.classList.add( 'hidden' );
        }

        if( !isContent6Hidden ) {
            content6.classList.add( 'hidden' );
        }
    });
});